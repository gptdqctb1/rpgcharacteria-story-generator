import {
  Controller,
  Get,
  Body,
  Query,
  Req,
  Res,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { HistoryService } from './history.service';
import { ApiBearerAuth } from '@nestjs/swagger';

export interface characterData {
  race: string;
  genre: string;
  classe: string;
  metier: string;
  caracteristic: string;
}
@Controller('api/url-api-description/v1/')
export class HistoryController {
  constructor(
    private chatGPTService: HistoryService,
  ) {}

  private readonly logger = new Logger(HistoryController.name);
  @ApiBearerAuth()
  @Get()
  async generateHistory(@Query() body: characterData, @Req() req, @Res() res) {
    // return res
    //   .status(HttpStatus.OK)
    //   .send({ name: 'blablablabla', description: 'blablablablablablablabla' });
    this.chatGPTService
      .generateText(body)
      .then((result) => {
        this.logger.log('GET: ' + JSON.stringify(result));
        return res.status(HttpStatus.OK).send(result);
      })
      .catch((e) => {
        this.logger.error('Error: ' + e);
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(e);
      });
  }
}
