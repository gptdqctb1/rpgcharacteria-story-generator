import { Injectable } from '@nestjs/common';
import { Configuration, OpenAIApi } from 'openai';
import { characterData } from './history.controller';

@Injectable()
export class HistoryService {
  private readonly openai: OpenAIApi;

  constructor() {
    const configuration = new Configuration({
      apiKey: process.env.OPENAI_API_KEY,
    });
    this.openai = new OpenAIApi(configuration);
  }

  async callApi(prompt: string): Promise<any> {
    return await this.openai.createCompletion({
      prompt: prompt,
      model: 'text-davinci-003',
      temperature: 0.3,
      max_tokens: 500,
      frequency_penalty: 0.16,
      presence_penalty: 0.63,
    });
  }

  async generateText(body: characterData): Promise<any> {
    // eslint-disable-next-line prefer-const
    let prompts: { [key: string]: string } = {};
    try {
      const promptName = `On est dans dans un jeu de rôle, invente moi le nom d'un personnage de sexe: ${body.genre} qui est ${body.caracteristic}.Il a pour race ${body.race}. Je veux seulement le nom dans ta réponse`;
      const resPromptName = await this.callApi(promptName);
      prompts.name = resPromptName.data.choices[0].text
        .replace(/\n\n/g, '')
        .replace('.', '');
      const promptGender = `On est dans dans un jeu de rôle, le personnage a pour nom ${prompts.name} de sexe: ${body.genre}. Il a pour race ${body.race}. Il est ${body.caracteristic}. Il a pour classe ${body.classe}. Il a pour métier ${body.metier}. Génère moi l'histoire du personnage en maximum 500 caratères`;
      const resPromptGender = await this.callApi(promptGender);
      prompts.description = resPromptGender.data.choices[0].text.replace(
        /\n\n/g,
        '',
      );
      return prompts;
    } catch (e) {
      throw new Error(e);
    }
  }
}
