import { Injectable } from '@nestjs/common';
import * as firebase from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';
import { mockPrivateKey } from './mock/firebase-mock';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

const serviceAccount: ServiceAccount = {
  type: 'service_account',
  project_id: 'character-builder-22fbe',
  private_key_id:
    process.env.NODE_ENV === 'test'
      ? 'test'
      : process.env.PRIVATE_KEY_ID_FIREBASE,
  private_key:
    process.env.NODE_ENV === 'test'
      ? mockPrivateKey
      : process.env.PRIVATE_KEY_FIREBASE.replace(/\\n/g, '\n'),
  client_email:
    process.env.NODE_ENV === 'test'
      ? 'test'
      : process.env.CLIENT_EMAIL_FIREBASE,
  client_id:
    process.env.NODE_ENV === 'test' ? 'test' : process.env.CLIENT_ID_FIREBASE,
  auth_uri: 'https://accounts.google.com/o/oauth2/auth',
  token_uri: 'https://oauth2.googleapis.com/token',
  auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
  client_x509_cert_url:
    'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-9e9g9%40inaro-auth.iam.gserviceaccount.com',
} as ServiceAccount;

@Injectable()
export class FirebaseAdminService {
  private readonly appFirebase: firebase.app.App;
  constructor() {
    this.appFirebase = firebase.initializeApp(
      {
        credential: firebase.credential.cert(serviceAccount),
      },
      'user',
    );
  }

  getAppFirebase(): firebase.app.App {
    return this.appFirebase;
  }
}
