# Image finale
FROM node:14-alpine

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
ENV PORT=3002
EXPOSE $PORT
CMD ["npm", "run", "start"]
